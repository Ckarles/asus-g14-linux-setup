from pathlib import Path
import typing as t

import uefi_firmware
import typer
import typing_extensions as tx


app = typer.Typer()

@app.command()
def main(uefi_rom: tx.Annotated[Path, typer.Argument(
    exists = True,
    readable = True,
    resolve_path = True,
)]):
    rom = uefi_rom.read_bytes()
    parser = uefi_firmware.AutoParser(rom)
    if parser.type() != 'unknown':
        firmware = parser.parse()
        firmware.showinfo()

if __name__ == "__main__":
    app()
