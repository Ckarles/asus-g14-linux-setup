# asus-g14-linux

> Setup of an asus g14 2022 (GA402RK)
> Thanks to the asus-linux community and krst in particular, you are awesome.

This document contains personal notes about a new asus g14 2022 complete setup featuring Fedora 40, KDE, win11 guest and runtime switch of the dGPU between the win11 VM and Linux host.

If you exactly follow this document as of this date, you will end up with the exact same system that I'm running on right now, however these are mostly personal notes, most of the simple steps are not explained command-by-command. If you're a reader of this document and you'd like some parts to be precised, or if you have any feedback of any sort, feel free to reach out to me me through the asus-linux discord.

Unless precised so, all the commands in this document must be ran as root.

## Install Fedora 40 Everything

Download one of the best Fedora release, [Fedora Everything](https://alt.fedoraproject.org/)

Default Automatic partitioning with encryption.
This sets up a btrfs partition encrypted with LUKS2.

Package selection during install:
Fedora Custom Operating System.
- Administration Tools
- System Tools
- NetworkManager Submodules

Boot the system. Install with `dnf install`
- @kde-desktop-environment
- @firefox
- @virtualization


Setup DM and DE. [ref.](https://docs.fedoraproject.org/en-US/quick-docs/switching-desktop-environments/)
```bash
cat << EOF > /etc/sysconfig/desktop
DESKTOP="KDE"
DISPLAYMANAGER="KDE"
EOF
```

Change the default runlevel to graphical and move up there.
```bash
systemctl set-default graphical.target
systemctl isolate graphical.target
```


## Setup btrfs snapshots

btrfs snapshots are very helpful to setup asap, as from now on we can snapshot before every change and revert if necessary, like in a VCS. [ref.](https://btrfs.wiki.kernel.org/index.php/SysadminGuide#Managing_Snapshots)

```bash
# With "xxx" as the name of the system

# 1. Create the "snapshots" subvolume
mkdir /mnt/btrfs
mount -o subvol=/ /dev/disk/by-label/fedora_xxx /mnt/btrfs
btrfs subvolume create /mnt/btrfs/snapshots
umount /mnt/btrfs
rmdir /mnt/btrfs

# 2. Add the volume to the fstab and mount it
mkdir /mnt/snapshots
dev_uuid=$(blkid -o export /dev/disk/by-label/fedora_* | grep ^UUID= | cut -d"=" -f2)
cat << EOF >> /etc/fstab
UUID=$dev_uuid /mnt/snapshots btrfs subvol=snapshots,compress=zstd:1,x-systemd.device-timeout=0 0 0
EOF
systemctl daemon-reload
mount /mnt/snapshots

# 3. Create snapshot directories and perform the initial snapshots
btrfs subvolume snapshot -r / /mnt/snapshots/snapshot_root_$(date -u -I)_01_init
btrfs subvolume snapshot -r /home /mnt/snapshots/snpashot_home_$(date -u -I)_01_init
```
We can now snapshot after after every change to keep track and rollback whenever necessary.

Btrfs has important performance issues while handling virtualized filesystem. We're going to have a win11 VM with GPU passthrough and this would be detrimental. We'll loose some features but these changes are necessary to avoid performance and fragmentation issues.

```bash
# Remove COW for future VM images
chattr +C /var/lib/libvirt/images
```


## Install Hardware packages

[ref.](https://asus-linux.org)
Install asusd / asusctl then `enable` and start the `asusd` service.


## Touchpad _tap_ in x11 sddm

sddm is running as X11 by default, and doesn't support the touchpad tap as click by default.
[ref.](https://www.reddit.com/r/kde/comments/qgnoav/comment/hi7yeri/?utm_source=share&utm_medium=web2x&context=3)

```bash
cat << "EOF" > /etc/X11/xorg.conf.d/10-touchpad.conf
Section "InputClass"
Identifier "MyTouchpad"
MatchIsTouchpad "on"
Driver "libinput"
Option "Tapping" "on"
Option "MiddleEmulation" "on"
Option "DisableWhileTyping" "on"
EndSection
EOF
```


## Profile and led mode switch

For now, just add `fn + F4` and `fn + F5` respectively to `asusctl led-mode -n` and `asusctl profile -n` in the KDE shortcuts.


## Disable Resizable BAR

[ref.](https://gist.github.com/dixyes/740018e040593ef0ec729a784f84f8c7#prepare)

Resizable BAR is a pci feature which is going to cause our VM win11 some problems, we have to disable it. This setting is in the UEFI and ours is pretty limited in features, therefore to change this setting we're going to have to make a risky manipulation.

Update the bios (if not already done). If on the bios `GA402RKAS.317`, skip step 1 and directly follow step 2 "Disable Resizable BAR in UEFI".

Remember each new UEFI upgrades will override these modification so it's needed to do repeat these steps after each UEFI Flash.


#### Step 1: Find the setting in the UEFI

Download and unzip [UEFITool](https://github.com/LongSoft/UEFITool) and [
IFRExtractor-RS](https://github.com/LongSoft/IFRExtractor-RS). UEFITool will require to install `qt6-qtbase-gui` as dependency.

With UEFITool, open the same bios the machine is running with, then find `BAR`. Only one file entry should be returned, extract it then quit UEFITool. use ifrextractor to get a readable file from the `.efi`.
```bash
./ifrextractor Section_PE32_image_PciDynamicSetup_PciDynamicSetup_body.efi all
```
Open the resulting `.txt` file and look for the `Re-Size BAR Support` `CheckBox Prompt`. We'll need to extract the following information from this file:
```
OFFSET: The `VarOffset:` value indicted on the Re-Size BAR Support CheckBox Prompt
VALUE: Indicated by `Value:` this value should be 1 (as in, Resizable BAR is enabled)
VALUE_SIZE: Our value is a boolean, so that should be 0x1 (as in, the size of one bit)
VAR_NAME: The `Name:` of the VarStore container (declared earlier in the file). If not sure, the CheckBox Prompt have a `VarStoreId:` check for a VarStore declaration with the same `VarStoreId:` and get its `Name:`. This should be `PCI_COMMON`.
```


#### Step 2: Disable Resizable BAR in UEFI

Download [setup_var.efi](https://github.com/datasone/setup_var.efi). Be careful when using this tool, any mistake can be fatal.

Put `setup_var.efi` on a fat32 partition on a usb drive. Boot on systemrescuecd and start an efi shell from there. Find the right disk and call the program from there, e.g. `cd FS2:\`.

Use the variables gathered in Step 1, or if running on bios `GA402RK` version `317`, just use these ones:
```
OFFSET: 0x4
VALUE_SIZE: 0x1
VAR_NAME: PCI_COMMON
```
The current value is `0x1`, first confirm that it's really `0x1` then switch it to `0x0`.
```
# Check the current value is 1
setup_var.efi <OFFSET> -s <VALUE_SIZE> -n <VAR_NAME>
#> Read value in PCI_COMMON at offset 0x4 with 0x1 bytes: 0x01

# Change the value to 0
setup_var.efi <OFFSET> 0x0 -s <VALUE_SIZE> -n <VAR_NAME>
#> Written value in PCI_COMMON at offset 0x5 with 0x1 bytes: 0x0
```

Take the opportunity to enable "SR-IOV Support" the same way (for this bios it would be on 0x5).


## Setup dGPU to switch between Linux and win11 VM passtrough

We want the perfect setup, and on that machine, we can! (That's why you bought it, right ?) This setup will allow us to use the dGPU either from the Linux host or the Windows guest, and make this switch by starting or stopping our VM without having to logout or restart, all in a single session runtime.

### Preparations

Let's locate our dGPU.
```bash
lspci | grep -i vga

#> 03:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Navi 23 [Radeon RX 6650 XT / 6700S / 6800S] (rev c0)
#> 07:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Rembrandt [Radeon 680M] (rev c7)
```
Our dGPU is located in `03:00.0`, while the iGPU is on `07:00.0`.

Let's find what else is there.
```bash
lspci -s "03:"
#> 03:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Navi 23 [Radeon RX 6650 XT / 6700S / 6800S] (rev c0)
#> 03:00.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Navi 21/23 HDMI/DP Audio Controller
```
`03:00.1` is the audio card of the dGPU, we'll also need it later.


### amdgpu driver bug alleviation

Binding/unbiding the dGPU to/from amdgpu drive is causing a segfault and system freeze on some DE configuration. [This](https://www.reddit.com/r/VFIO/comments/ry4i4d/workaround_for_sysfs_cannot_create_duplicate/) workaround alleviates the issue by blocking any process which would want to use the dGPU if not launched with the group `passthru`.

```bash
boot_vga=$(cat "/sys/bus/pci/devices/0000:03:00.0/boot_vga")

cat <<EOF > /etc/udev/rules.d/72-passthrough.rules
KERNEL=="card[0-9]", SUBSYSTEM=="drm", SUBSYSTEMS=="pci", ATTRS{boot_vga}=="${boot_vga}", GROUP="passthru", TAG="nothing", ENV{ID_SEAT}="none"
KERNEL=="renderD12[0-9]", SUBSYSTEM=="drm", SUBSYSTEMS=="pci", ATTRS{boot_vga}=="${boot_vga}", GROUP="passthru", MODE="0660"
EOF
```
After reboot, the group change should be visible in `/dev/dri`.


Add the ability to execute a command with the passthru group via `sudo` by editing the sudoers file.
```bash
# /etc/sudoers

%wheel  ALL=(ALL : passthru)    NOPASSWD: ALL
```

Finally, add a helper to launch processes with the dGPU:
```bash
cat << "EOF" > /usr/local/sbin/exec-dgpu
#!/usr/bin/env bash

DRI_PRIME=1 sudo --preserve-env -g passthru env "PATH=$PATH" "$@"
EOF

chmod a+x /usr/local/sbin/exec-dgpu
```

With these changes, while the dGPU is not passed through our future win11 VM, we can use it for any process by simply calling `exec-dgpu <program>`.


### Runtime driver switch (vfio <-> amdgpu)

Reboot to apply all the changes we modifed before permanently.
We'll create 3 scripts that will help us to automate the dGPU switch between amdgpu <-> vfio-pci.

- The first script validate that nothing is currently using the dGPU, as unbinding it from its driver while in use will have unexpected consequences.
```bash
cat << "EOF" > /usr/local/sbin/validate_dgpu_is_unused
#!/usr/bin/env bash

set -e

pci_device="${1?param missing - pci device.}"
if test ! -e /sys/bus/pci/devices/"${pci_device}"; then
        echo "\"${pci_device}\" is not a valid pci device." >&2
        exit 2
fi

function validate_dri_is_unused () {
        dri_type="${1}"
        if userlist=$(fuser -v /dev/dri/by-path/pci-"${pci_device}"-"${dri_type}" 2>&1); then
                echo "GPU is being used." >&2
                echo "${userlist}" >&2
                exit 1
        fi
}

validate_dri_is_unused card
validate_dri_is_unused render
EOF

chmod 700 /usr/local/sbin/validate_dgpu_is_unused
```

- The second script binds a device to a driver, unbinding it from its current one if neccessary.
```bash
cat << "EOF" > /usr/local/sbin/bind-device-to-driver
#!/usr/bin/env bash

set -e

pci_device="${1?param missing - pci device.}"
if test ! -e /sys/bus/pci/devices/"${pci_device}"; then
        echo "\"${pci_device}\" is not a valid pci device." >&2
        exit 2
fi

driver="${2?param missing - driver.}"
if ! modinfo "${driver}" >/dev/null; then
        echo "\"${driver}\" is not a valid driver." >&2
        exit 2
fi

function add_driver_module () {
        module=$(modinfo -F name "${driver}")
        if ! (lsmod || true) | grep "^${module} " >/dev/null; then
                echo "Add module \"${module}\"." >&2
                modprobe "${module}"
        fi
}

function add_new_id_to_driver () {
        vendor=$(cat /sys/bus/pci/devices/"${pci_device}"/vendor)
        device=$(cat /sys/bus/pci/devices/"${pci_device}"/device)

        if ! ret=$((echo "${vendor} ${device}" > /sys/bus/pci/drivers/"${driver}"/new_id) 2>&1); then
                if [[ ! "${ret}" =~ "write error: File exists" ]]; then
                        echo "${ret}" >&2
                        exit 1
                fi
                return 0
        fi
        echo "Add new id \"${vendor} ${device}\" to driver \"${driver}\"." >&2
}

function is_device_bound_to_driver () {
        current_driver_path=$(readlink -f /sys/bus/pci/devices/"${pci_device}"/driver)
        current_driver=${current_driver_path##*/}
        if [[ "${current_driver}" = "${driver}" ]]; then
                return 0
        fi
        return 1
}

function unbind_from_current_driver () {
        if test ! -L /sys/bus/pci/devices/"${pci_device}"/driver; then
                echo "Device \"${pci_device}\" already free, skip unbind." >&2
                return 0
        fi

        if is_device_bound_to_driver; then
                echo "Device \"${pci_device}\" already bound to driver \"${driver}\", nothing to do here." >&2
                exit 0
        fi

        echo "Unbind device \"${pci_device}\"." >&2
        echo "${pci_device}" > /sys/bus/pci/devices/"${pci_device}"/driver/unbind
}

function bind_to_driver () {
        echo "Bind device \"${pci_device}\" to driver \"${driver}\"." >&2
        echo "${pci_device}" > /sys/bus/pci/drivers/"${driver}"/bind
}

function validate_device_is_bound_to_driver () {
        if ! is_device_bound_to_driver; then
                echo "Device\"${pci_device}\" is not bound to driver \"${driver}\"." >&2
                exit 1
        fi
}

add_driver_module

add_new_id_to_driver
unbind_from_current_driver
bind_to_driver

validate_device_is_bound_to_driver
EOF

chmod 700 /usr/local/sbin/bind-device-to-driver
```

The third script is calling the 2 others to change mode between hybrid mode and vfio mode.
```bash
cat << "EOF" > /usr/local/sbin/switch-dgpu-mode
#!/usr/bin/env bash

set -e

VIDEO_DEVICE="0000:03:00.0"
VIDEO_DRIVER="amdgpu"

AUDIO_DEVICE="0000:03:00.1"
AUDIO_DRIVER="snd_hda_intel"


mode="${1?param missing - dGPU mode.}"
if [[ "${mode}" = "hybrid" ]]; then
        target_video_driver="${VIDEO_DRIVER}"
        target_audio_driver="${AUDIO_DRIVER}"

elif [[ "${mode}" = "vfio" ]]; then
        target_video_driver="vfio-pci"
        target_audio_driver="vfio-pci"

else
        echo "Mode \"${mode}\" is not supported." >&2
        exit 2
fi

validate_dgpu_is_unused "${VIDEO_DEVICE}"

bind-device-to-driver "${VIDEO_DEVICE}" "${target_video_driver}"
bind-device-to-driver "${AUDIO_DEVICE}" "${target_audio_driver}"
EOF

chmod 700 /usr/local/sbin/switch-dgpu-mode
```

We can now switch our dGPU betwen vfio mode and hybrid mode. E.g. to switch from the (default) hybrid mode to the vfio mode `switch-dgpu-mode vfio`.


### Automate dGPU hybrid <-> vfio switch for VM passthrough

[ref.](https://passthroughpo.st/simple-per-vm-libvirt-hooks-with-the-vfio-tools-hook-helper/)

The dGPU mode will switch to vfio mode before the VM starts and switch back to hybrid mode after it stops.

Setup PassthroughPOST libvirt hooks manager.
```bash
mkdir -p /etc/libvirt/hooks
wget 'https://raw.githubusercontent.com/PassthroughPOST/VFIO-Tools/master/libvirt_hooks/qemu' -O /etc/libvirt/hooks/qemu
chmod +x /etc/libvirt/hooks/qemu
service libvirtd restart
```

Add the hooks.
```bash
mkdir -p /etc/libvirt/hooks/qemu.d/win11/{prepare/begin,release/end}

cat << "EOF" > /etc/libvirt/hooks/qemu.d/win11/prepare/begin/10-swich-dgpu-to-mode-vfio
#!/usr/bin/env bash

switch-dgpu-mode vfio
EOF

chmod +x /etc/libvirt/hooks/qemu.d/win11/prepare/begin/10-swich-dgpu-to-mode-vfio

cat << "EOF" > /etc/libvirt/hooks/qemu.d/win11/release/end/40-swich-dgpu-to-mode-hybrid
#!/usr/bin/env bash

switch-dgpu-mode hybrid
EOF

chmod +x /etc/libvirt/hooks/qemu.d/win11/release/end/40-swich-dgpu-to-mode-hybrid
```


### Create VM volume

To avoid fragmentation with qcow2 on btrfs, use a raw volume with preallocation.
```bash
qemu-img create /var/lib/libvirt/images/win11.img 80g
```


### Prepare virio drivers

We'll use virtio for performance reasons, however windows needs these drivers to even install so they are prepared in advance and mounted the iso during the first boot. [ref.](https://github.com/virtio-win/virtio-win-pkg-scripts/blob/master/README.md)

```bash
wget https://fedorapeople.org/groups/virt/virtio-win/virtio-win.repo -O /etc/yum.repos.d/virtio-win.repo
dnf config-manager --set-disabled virtio-win-stable
dnf config-manager --set-enabled virtio-win-latest

dnf update
dnf install virtio-win
```


### Create windows 11 VM guest with gpu passthrough

It would take a lot of time and effort to setup a proper app for the guest install and management. In the meantime, this is a copy-paste of one of the main resource used here: https://asus-linux.org/wiki/vfio-guide

```bash
sudo usermod -aG libvirt,kvm <user>
```

Start and enable libvirtd.
```bash
systemctl start libvirtd
systemctl enable libvirtd
```

```bash
cat << "EOF" > /etc/libvirt/hooks/qemu.d/win11/prepare/begin/20-reserve-hugepages.sh
#!/usr/bin/env bash

## Load VM variables
source "/etc/libvirt/hooks/qemu.d/win11/vm-vars.conf"

# Calculate number of hugepages to allocate from memory (in MB)
#TODO GET THE VM MEMORY
HUGEPAGES="$(($VM_MEMORY/$(($(grep Hugepagesize /proc/meminfo | awk '{print $2}')))))"

echo "Allocating hugepages at 2048 KiB per page..."
echo $HUGEPAGES > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages
ALLOC_PAGES=$(cat /proc/sys/vm/nr_hugepages)


## If successful, notify user
if [ "$ALLOC_PAGES" -eq "$HUGEPAGES" ]
then
    echo "Succesfully allocated $ALLOC_PAGES / $HUGEPAGES pages!"
fi


## Drop caches to free up memory for hugepages if not successful
if [ "$ALLOC_PAGES" -ne "$HUGEPAGES" ]
then
    echo 3 > /proc/sys/vm/drop_caches
fi

## If not successful, try up to 10000 times to allocate
TRIES=0
while (( $ALLOC_PAGES != $HUGEPAGES && $TRIES < 1000 ))
do
    ## Defrag RAM then try to allocate pages again
    echo 1 > /proc/sys/vm/compact_memory
    echo $HUGEPAGES > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages
    ALLOC_PAGES=$(cat /proc/sys/vm/nr_hugepages)
    ## If successful, notify user
    echo "Succesfully allocated $ALLOC_PAGES / $HUGEPAGES pages!"
    let TRIES+=1
done

## If still unable to allocate all requested pages, revert hugepages and quit
if [ "$ALLOC_PAGES" -ne "$HUGEPAGES" ]
then
    echo "Not able to allocate all hugepages. Reverting..."
    echo 0 > /proc/sys/vm/nr_hugepages
    exit 1
fi
EOF
sudo chmod +x /etc/libvirt/hooks/qemu.d/win11/prepare/begin/20-reserve-hugepages.sh

cat  << "EOF" > /etc/libvirt/hooks/qemu.d/win11/release/end/10-release-hugepages.sh
#!/usr/bin/env bash

## Load VM variables
source "/etc/libvirt/hooks/qemu.d/win11/vm-vars.conf"

## Remove Hugepages
echo "Releasing hugepage memory back to the host..."
echo 0 > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages

## Advise if successful
ALLOC_PAGES=$(cat /proc/sys/vm/nr_hugepages)

if [ "$ALLOC_PAGES" -eq 0 ]
then
    echo "Memory successfully released!"
fi
EOF
sudo chmod +x /etc/libvirt/hooks/qemu.d/win11/release/end/10-release-hugepages.sh
```

TODO: Redo this whole section
```bash
cat << "EOF" > /etc/libvirt/hooks/qemu.d/win11/prepare/begin/40-isolate-cpus.sh
#!/usr/bin/env bash

## Load VM variables
source "/etc/libvirt/hooks/qemu.d/win11/vm-vars.conf"

## Isolate CPU cores as per set variable
systemctl set-property --runtime -- user.slice AllowedCPUs=$VM_ISOLATED_CPUS
systemctl set-property --runtime -- system.slice AllowedCPUs=$VM_ISOLATED_CPUS
systemctl set-property --runtime -- init.scope AllowedCPUs=$VM_ISOLATED_CPUS
EOF

sudo chmod +x /etc/libvirt/hooks/qemu.d/win11/prepare/begin/40-isolate-cpus.sh

cat << "EOF" > /etc/libvirt/hooks/qemu.d/win11/release/end/20-return-cpus.sh
#!/usr/bin/env bash

## Load VM variables
source "/etc/libvirt/hooks/qemu.d/win11/vm-vars.conf"

## Return CPU cores as per set variable
systemctl set-property --runtime -- user.slice AllowedCPUs=$SYS_TOTAL_CPUS
systemctl set-property --runtime -- system.slice AllowedCPUs=$SYS_TOTAL_CPUS
systemctl set-property --runtime -- init.scope AllowedCPUs=$SYS_TOTAL_CPUS
EOF

sudo chmod +x /etc/libvirt/hooks/qemu.d/win11/release/end/20-return-cpus.sh
```

```bash
cat << "EOF" > /etc/libvirt/hooks/qemu.d/win11/vm-vars.conf
## win11 VM Script Parameters

# How much memory we've assigned to the VM, in kibibytes

VM_MEMORY=8388608

# Set which CPU's to isolate, and your system's total
# CPU's. For example, an 8-core, 16-thread processor has
# 16 CPU's to the system, numbered 0-15. For a 6-core,
# 12-thread processor, 0-11. The SYS_TOTAL_CPUS variable
# should always reflect this.
#
# You can define these as a range, a list, or both. I've
# included some examples:
#
# EXAMPLE=0-3,8-11
# EXAMPLE=0,4,8,12
# EXAMPLE=0-3,8,11,12-15
VM_ISOLATED_CPUS=0-7
SYS_TOTAL_CPUS=0-15
EOF
```


```bash
virt-install \
  --connect qemu:///system \
  --name win11 --osinfo win11 \
  --memory memory=8192 \
  --memorybacking hugepages=on \
  --cpu host-passthrough,\
topology.sockets=1,\
topology.cores=4,\
topology.threads=2,\
cache.mode=passthrough,\
require=topoext \
  --sysinfo host \
  --cputune \
vcpupin0.vcpu=0,vcpupin0.cpuset=8,\
vcpupin1.vcpu=1,vcpupin1.cpuset=9,\
vcpupin2.vcpu=2,vcpupin2.cpuset=10,\
vcpupin3.vcpu=3,vcpupin3.cpuset=11,\
vcpupin4.vcpu=4,vcpupin4.cpuset=12,\
vcpupin5.vcpu=5,vcpupin5.cpuset=13,\
vcpupin6.vcpu=6,vcpupin6.cpuset=14,\
vcpupin7.vcpu=7,vcpupin7.cpuset=15,\
emulatorpin.cpuset=0-7,\
iothreadpin0.iothread=1,\
iothreadpin0.cpuset=0-7 \
  --iothreads 1 \
  --vcpus placement=static \
  --cdrom $HOME/Downloads/en-us_windows_11_business_editions_version_22h2_updated_sep_2022_x64_dvd_840da535.iso \
  --features \
smm.state=on,\
hyperv.synic.state=on,\
hyperv.reset.state=on,\
xpath0.create=./hyperv/vpindex,xpath1.set=./hyperv/vpindex/@state=on,\
xpath2.create=./hyperv/stimer,xpath3.set=./hyperv/stimer/@state=on,\
xpath4.create=./hyperv/frequencies,xpath5.set=./hyperv/frequencies/@state=on \
  --memballoon none \
  --qemu-commandline "-overcommit cpu-pm=on" \
  --boot \
loader=/usr/share/edk2/ovmf/OVMF_CODE.secboot.fd,\
loader.readonly=yes,\
loader.type=pflash,\
loader_secure=yes \
  --disk /var/lib/libvirt/images/win11.img,size=80,sparse=yes,format=raw,boot.order=1,target.bus=virtio \
  --disk /usr/share/virtio-win/virtio-win.iso,device=cdrom \
  --network network=default,model.type=virtio \
  --hostdev 03:00.0 \
  --hostdev 03:00.1
```

At VM boot, install the virtio drivers from the ISO (`NetKVM` & `viostor`), then install windows. At the windows configuration step, use `shift + F10` to open a terminal and `oobe\bypassnro` to avoid creating a fucking microsoft account. After the install, update the system, remove all the m$ bullcrap, activate windows (using a product key **of course**), and install the amd drivers.


### Looking glass setup

[ref.](https://looking-glass.io/docs/B6/install/)

Follow install dependency instructions [here](https://looking-glass.io/wiki/Installation_on_other_distributions#Fedora_35.2B).

Download looking-glass source code: https://looking-glass.io/downloads
Build and install looking-glass clientsemodule -i w11-guest-with-looking-glass.pp
```bash
mkdir client/build
cd client/build
cmake ../
make
make install
```

Install IVSHMEM module with dkms
```bash
cd module
dkms install "."
# (2560 * 1600 *4 * 2) / 2^20 + 10 = 41.25 -> 64MiB frame memory size

cat << "EOF" > /etc/udev/rules.d/99-kvmfr.rules
SUBSYSTEM=="kvmfr", OWNER="root", GROUP="kvm", MODE="0660"
EOF
```

Then add this bit to the domain xml
```xml
<qemu:commandline>
  <qemu:arg value='-device'/>
  <qemu:arg value='{"driver":"ivshmem-plain","id":"shmem0","memdev":"looking-glass"}'/>
  <qemu:arg value='-object'/>
  <qemu:arg value='{"qom-type":"memory-backend-file","id":"looking-glass","mem-path":"/dev/kvmfr0","size":67108864,"share":true}'/>
</qemu:commandline>
```

Allow these modifications in selinux
```bash
ausearch -c 'qemu-system-x86' --raw | audit2allow -M w11-guest-with-looking-glass
semodule -i w11-guest-with-looking-glass.pp
```

Install looking glass host on the win11 VM.

[ref.](https://asus-linux.org/wiki/vfio-guide/#using-looking-glass-with-a-virtual-display-driver)
Install [scoop](https://scoop.sh/) in the VM, then install [IddDisplayDriver](https://github.com/ge9/IddSampleDriver) with scoop.

Uncomment `cgroup_device_acl` in `/etc/libvirt/qemu.conf` and add "/dev/kvmfr0" to the list, then restart libvirtd. 

```bash
cat << "EOF" > /etc/libvirt/hooks/qemu.d/win11/prepare/begin/30-load-looking-glass-requirements.sh
#!usr/bin/env bash

modprobe kvmfr static_size_mb=64
EOF

chmod +x /etc/libvirt/hooks/qemu.d/win11/prepare/begin/30-load-looking-glass-requirements.sh

cat << "EOF" > /etc/libvirt/hooks/qemu.d/win11/release/end/30-kill-looking-glass-client.sh
#!/usr/bin/env bash

echo "Killing Looking Glass..."
killall looking-glass-client
modprobe -r kvmfr
EOF

chmod +x /etc/libvirt/hooks/qemu.d/win11/release/end/30-kill-looking-glass-client.sh
```

For now, start looking glass manually with `looking-glass-client -f /dev/kvmfr0 -F win:size=2560x1600`. Use `alt + F3 -> configure special application settings` to disable the compositor for that window. KWIN does not support disabling the desktop scale for a particular application sothe 1:1 scaling of the guest desktop will only work with the KDE global desktop scale set to 100%.

Setup a looking-glass shortcut
```bash
cat << "EOF" > /usr/local/sbin/looking-glass
#!/usr/bin/env bash

looking-glass-client -f /dev/kvmfr0 -F win:size=2560x1600
EOF

chmod a+x /usr/local/sbin/looking-glass
```

### Share between Fedora host and win11 guest

[ref.](https://virtio-fs.gitlab.io/)

Download [WinFsp](http://www.secfs.net/winfsp). Install the drivers for PCI device and viofs.
Copy `virtiofs.exe` from the drivers to `C:\Program Files\virtiofs`.
Add `<access mode='shared'/>` inside the `<memoryBacking>` section of `/etc/libvirt/qemu/win11.xml`.
Add the following in the `<devices>` section of `/etc/libvirt/qemu/win11.xml`.
```xml
<filesystem type='mount' accessmode='passthrough'>
      <driver type='virtiofs' queue='1024'/>
      <source dir='/srv/win11-storage'/>
      <target dir='shared_storage'/>
</filesystem>
```

Create the folder `mkdir /srv/win11-storage` and restart libvirtd.

```cmd
sc create VirtioFsSvc binpath="C:\Program Files\virtiofs\virtiofs.exe" start=auto type=own depend="WinFsp.Launcher/VirtioFsDrv" DisplayName="Virtio FS Service"
```

To test this integration without restarting: `/etc/libvirt/qemu/win11.xml`. The shared storage should show as `Z:` in win11.



# TODO
- [x] Fn+F3 "aura" key - for keyboard aura switch
- [x] Fn+F5 "profile" key - for profile switch
- [x] Disable resizable BAR for passthrough
- [ ] Set default editor to vim (fedora package to do that doesn't work with sudo)
- [x] Implement touchpad _tap_ in x11 sddm
- [x] VM optimization (cpu profile, cgroup...)
- [x] Guest mounted storage
- [x] VM install
- [x] Looking glass setup
- [ ] Undervolt dGPU / CPU
